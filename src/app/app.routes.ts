import { UserComponent } from './user/user.component';
import { SearchComponent } from './search/search.component';
import { CircularComponent } from './circular/circular.component';
import { RouterModule, Routes } from '@angular/router';

export const appRoutes: Routes = [
  {
    path: 'circular',
    component: CircularComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: '',
    component: SearchComponent
  },
  {
    path: 'user/:userId',
    component: UserComponent
  }
]
