import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-circular',
  templateUrl: './circular.component.html',
  styleUrls: ['./circular.component.css']
})
export class CircularComponent implements OnInit {

  public happyText = 'Manager is on Vacation! uhu';

  public myTodos = [
    'Wash coffee mug',
    'Take a Shower',
    'Say hi to the new hire'
  ];

  constructor() { }

  ngOnInit() {
    this.happyText = 'Manager is Sick!';
  }

  makeMeHappier() {
    this.happyText = 'Manager got fired!';
  }

}
