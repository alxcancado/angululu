import { Router } from '@angular/router';
import { GithubService } from './../github/github.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [GithubService]
})
export class SearchComponent implements OnInit {

  public searchText;
  public searchResults;
  public searchCount;

  constructor( private router: Router, private githubService: GithubService ) { }

  ngOnInit() {}

  onKeyup(event) {
    this.searchText = event.target.value;
  }

  getUsers() {
    this.githubService.getUser(this.searchText).subscribe(
      res => {
        this.searchResults = res;
        this.searchCount = res.total_count;
      }
    );
  }

  showUserDetail(user) {
    this.router.navigate(['user', user.login]);
  }

}
